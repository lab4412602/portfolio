# Tämän wokwi projektin tavoitteena oli tarkoitus tehdä IOT-laite joka seuraa kuvitteellisen kasvihuoneen jossa
# kasvatetaan tomaatteja olosuhteita ja lähettää tiedot palvelimelle josta ne ovat nähtävissä asiakasohjelman kautta.
# Microcontrolleri RASPBERRY PI PICO W
# Anturi: DHT22 KOSTEUS- JA LÄMPÖMITTAUSMODULI
# Näyttö: LCD 1602
# Laite: https://wokwi.com/projects/388873250192055297

# Tällä saadaan WiFi-verkkotoiminnot käyttöön
import network
# DHT-anturin kirjasto
import dht 
# Pinnien hallinta
from machine import I2C, Pin
# Sleep funktiolla saadaan toimintoja ajoitettua
from time import sleep
# urequests kirjasto HTTP pyyntöjen tekemiseen
import urequests as requests
# Tämä moduuli tarvitaan LCD-näytön käyttämiseen
from pico_i2c_lcd import I2cLcd

# Osoite johon data lähetetään
API_Adress = "http://137.135.189.222/api/"
# Wokwi wifin tiedot
SSID = "Wokwi-GUEST"
PASSWORD = ""
WLAN = network.WLAN(network.STA_IF)

# Wlan yhteyden muodostus
def connectWifi():
    # WLAN aktivointi
    WLAN.active(True)
    # Yhdistäminen - SSID - SALASANA
    WLAN.connect(SSID, PASSWORD)
    while not WLAN.isconnected():
        print(".", end="")
        sleep(0.1)
    # Yhteys muodostettu
    print("Connected!")
    print(WLAN.ifconfig())
    return None

# Luodaan I2C-objekti ja määritellään datan (SDA) ja kellon (SCL) nastat.
i2c = I2C(0, sda=Pin(0), scl=Pin(1), freq=400000)

# Haetaan I2C osoite
I2C_ADDR = i2c.scan()[0]

# Luodaan lcd-oblekti ja määritetään näytön rivien ja sarakkeiden määrä
lcd = I2cLcd(i2c, I2C_ADDR, 2, 16)

# Määritellään led valon pinnit
led = Pin(9, Pin.OUT)

# Luetaan data sensorilta
def operate():
    # Kerrotaan mihin pinniin sensori on yhdistetty
    sensor = dht.DHT22(Pin(28))
    # Looppi joka lukee datan määritellyn ajan välein, tulostaa arvot ja lähettää ne tietokantaan
    while True:
        try:
            # Haetaan data anturilta
            sensor.measure()
            # Tyhjennetään näyttö ennen uusia tulostuksia
            lcd.clear()
            

            # Lämpötila anturilta
            # Optimaalinen lämpötila tomaattien kasvatukseen on 20-25°C
            temp = sensor.temperature()

            # Tulostetaan lämpötila konsoliin ja varoitus mikäli lämpötila ei ole oikealla alueella
            if temp < 20:
                print(f"Temperature: {temp}°C ALERT! Temperature is too cold!")
            elif temp > 25:
                print(f"Temperature: {temp}°C ALERT! Temperature is too hot!")
            else:
                print(f"Temperature: {temp}°C")
            #Tulostetaan lämpötila näytölle
            lcd.putstr(f"Temp: {temp} C")
            
            # Ilmankosteus anturilta
            # Optimaalinen ilmankosteus tomaattien kasvatukseen on 60-80%
            # Tulostetaan ilmankosteus ja varoitus mikäli ilmankosteus ei ole oikealla alueella
            # Tulostetaan ilmankosteus näytölle
            humid = sensor.humidity()
            if humid < 60:
                print(f"Humidity: {humid}% ALERT! Humidity is too low!")
            elif humid > 80:
                print(f"Humidity: {humid}% ALERT! Humidity is too high!")
            else:
                print(f"Humidity: {humid}%")
            
            # Siirretään kursori toisen rivin alkuun
            lcd.move_to(0,1)

            # Tulostetaan ilmankosteus näytölle
            lcd.putstr(f"Humidity: {humid} %")

            # Jos lämpötila tai ilmankosteus ei ole sallituissa rajoissa sytytetään varoitusvalo
            if temp < 20 or temp > 25 or humid < 60 or humid > 80:
                led.on()
            else:
                led.off()

            # Datan lähetys palvelimelle
            sendData(temp, humid)

        # Jos anturi ei ole yhdistetty laitteeseen, tulostetaan error
        except OSError as e:
            print('Failed to read sensor.')

# Datan lähetys
def sendData(temp, humid):
    try:
        # Lähetetään GET-pyyntö jonka mukana data siirtyy palvelimelle
        response = requests.get(f"{API_Adress}/data_add?temp={temp}&humid={humid}")
        # Tarkastetaan että pyyntö on mennyt perille oikein
        if response.status_code == 200:
            print("Sensor data sent succesfully to Azure server.")
            print(response.json())
        response.close()
    # Tulostetaan virheilmoitus jos data ei ole mennyt palvelimelle
    except:
        print("Could not send data...")
    return None



def main():
    # Järjestelmän käynnistys ja yhdistys wifi-verkkoon
    print("System starting...")
    print("Connecting to WiFI.", end="")
    connectWifi()
    # Datan lukeminen, tulostus ja lähetys
    print("Reading data:")
    operate()

main()