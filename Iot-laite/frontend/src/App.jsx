// Importataan useSate ja useEffect hookit käytettäväksi
import { useState, useEffect } from 'react'
// Importataan css määrittelyt
import './App.css'

function App() {
  // Alustetaan data muuttuja
  const [data, setData] = useState({temp: 0, humid: 0});

  // Haetaan data palvelimelta
  const fetchData = async () => {
    // Yritetään hakea data palvelimelta ja asetetaan se data muuttujaan
    try {
      const response = await fetch("http://137.135.189.222/api/data");
      const jsonData = await response.json();
      setData(jsonData);
    // Jos datan hakeminen epäonnistuu tulostetaan virheilmoitus
    } catch (error) {
      console.error("Virhe datan hakemisessa:", error);
    }
  }

    // Ajetaan fetchData-funktio heti ja sitten joka 10. sekunti
    useEffect(() => {
      // Haetaan data palvelimelta
      fetchData();
      const intervalId = setInterval(fetchData, 10000);
  
      // Nollataan laskuri datan hakemisen jälkeen
      return () => clearInterval(intervalId);
    }, []);
  
  // Määritellään getBackgroundColorTemp funktio joka palauttaa taustan värin lämpötilan mukaan
  const getBackgroundColorTemp = (temp) => {
    // Määritä väri temp-arvon perusteella
    if (temp < 20) {
      return "blue"; // Sininen taustaväri alle 20 asteen lämpötilalle, kasvihuone on liian kylmä
    } else if (temp < 21) {
      return "yellow"; // Keltainen taustaväri 20-21 asteen lämpötilalle kun lähestytään alarajaa
    } else if (temp < 24) {
      return "green"; // Vihreä taustaväri yli 21-24 asteen lämpötilalle optimaalinen olosuhde
    } else if (temp < 25) {
      return "yellow"; // Keltainen taustaväri 24-25 asteen lämpötilalle kun lähestytään ylärajaa
    } else {
      return "red"; //Punainen taustaväri yli 25 asteen lämpötilalle, kasvuhuone on liian kuuma
    }
  };

  // Määritellään getFontColorTemp funktio joka muuttaa tekstin mustaksi kun taustan väri on keltainen
  // luettavuuden helpottamiseksi
  const getFontColorTemp = (temp) => {
    // Jos taustaväri on keltainen muutetaan fontti mustaksi
    if (temp > 20 && temp < 21 || temp > 24 && temp < 25) {
      return "black";
    } else {
      return "white"; // muussa tapauksessa fontin väri pysyy valkoisena
    }
  };

    // Määritellään getBackgroundColorHumidity funktio joka palauttaa taustan värin ilmankosteuden mukaan
  const getBackgroundColorHumidity = (humid) => {
    if (humid < 60) {
      return "red"; // Punainen taustaväri kun ilmankosteus on liian pieni
    } else if (humid < 62) {
      return "yellow"; // Keltainen taustaväri kun ilmankosteus lähestyy raja-arvoa
    } else if (humid < 78) {
      return "green"; // Vihreä taustaväri kun ollaan optimaalisella alueella
    } else if (humid < 80) {
      return "yellow"; // Keltainen väri kun ilmankosteus lähestyy raja-arvoa
    } else {
      return "red"; // Punainen väri kun ilmankosteus on liian suuri
    }
  };

  // Määritellään getFontColorHumidity funktio joka muuttaa tekstin mustaksi kun taustan väri on keltainen
  // luettavuuden helpottamiseksi
  const getFontColorHumidity = (humid) => {
    // Jos taustaväri on keltainen muutetaan fontti mustaksi
    if (humid > 60 && humid < 62 || humid > 78 && humid < 80) {
      return "black";
    } else {
      return "white"; // muussa tapauksessa fontti pysyy valkoisena
    }
  };

  // Palautetaan sivuilla näkyvät asiat
  return (
    <>
      {/* Jokaiselle kasvihuoneelle nimetään oma div jolloin sivuille saa helposti näkymään useamman kasvihuoneen tiedot */}
      <div className="greenhouse1">
        {/* Kasvihuoneen nimi ja tieto-laatikoiden otsikot kohdistettuna välilyönneillä oikealle kohdalle */}
        <h1>Kasvihuone 1</h1>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;Lämpötila &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ilmankosteus</p>
        <div className="info">
          {/* Lämpötilan näyttö, jossa taustaväri ja fonttiväri määritellään arvojen mukaan, jolloin näkee helposti onko kaikki kohdallaan */}
          <div className="temp" style={{ backgroundColor: getBackgroundColorTemp(data.temp), color: getFontColorTemp(data.temp)}}>
            <h2>{data.temp}°C</h2>
          </div>
          {/* Ilmankosteuden näyttö, jossa taustaväri ja fonttiväri määritellään arvojen mukaan, jolloin näkee helposti onko kaikki kohdallaan */}
          <div className="humid" style={{ backgroundColor: getBackgroundColorHumidity(data.humid), color: getFontColorHumidity(data.humid)}}>
            <h2>{data.humid}%</h2>
          </div>
        </div>
      </div>
    </>
  )
}

export default App
