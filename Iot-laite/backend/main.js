// Otetaan käyttöön express kirjasto
const express = require('express');
// Luodaan sovelluksen instanssi
const app = express();
// Määritellään porttinumero, jolla palvelin kuuntelee pyyntöjä
const PORT = 3000;
// Tulostetaan konsoliin viesti joka kertoo palvelinohjelman käynnistymisestä
console.log("Server-side program starting")
// Alustetaan temp ja humid muuttujat
let temp = 0;
let humid = 0;


// Juurireitin käsittelijä GET-pyynnölle johon pyynnön lähettämällä voi tarkistaa onko palvelin käynnissä
app.get('/', (req, res) => {
    // req = request objekti, res = response objekti
    res.send('OK'); // Lähetetään vastauksena OK josta selviää että palvelin on päällä
});

// Määritellään reitti josta asiakasohjelma saa datan lähettämällä GET-pyynnön
app.get('/data', (req,res) => {
    res.json({ temp, humid }); // Lähetetään vastauksena temp ja humid arvot JSON-muodossa
});

// Määritellään reitti johon laiteohjelma lähettää datan GET-pyynnöllä
app.get('/data_add', (req,res) => {
    temp = parseFloat(req.query.temp); // Varmistetaan että data on numeraalista ja tallennetaan se muuttujaan
    humid = parseFloat(req.query.humid); // Varmistetaan että data on numeraalista ja tallennetaan se muuttujaan
    res.json({ temp, humid }); // Lähetetään vastauksena data takaisin jotta laiteohjelma tietää että data tuli perille oikein
});

// Käynnistetään Express-sovellus kuuntelemaan määriteltyä porttia
// Kun sovellus on käynnistetty, tulostetaan konsoliin viesti, joka osoittaa kuuntelemisen alkaneen
app.listen(PORT, () => console.log(`Listening on http://localhost:${PORT}`));
