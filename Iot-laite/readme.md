# Iot-kasvihuone projekti

Tämän projektin tavoitteena oli tehdä IOT-laite joka seuraa kuvitteellisen kasvihuoneen, jossa
kasvatetaan tomaatteja, olosuhteita ja lähettää tiedot tekemälleni azure-palvelimelle josta ne ovat nähtävissä reactilla toteutetun asiakasohjelman kautta.

- Microcontrolleri RASPBERRY PI PICO W
- Anturi: DHT22 KOSTEUS- JA LÄMPÖMITTAUSMODULI
- Näyttö: LCD 1602

[Laite wokwissa](https://wokwi.com/projects/388873250192055297)

